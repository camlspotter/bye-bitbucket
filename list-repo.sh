#!/bin/sh
URL="https://api.bitbucket.org/2.0/repositories/camlspotter"
rm -f repos.json

USER=`cat user.txt`

while true; do
    echo URL: \"$URL\"
    echo curl --request GET --user "$USER" "$URL"
    curl --request GET --user "$USER" "$URL" | jq . > repos.json.tmp
    URL=`jq -r .next < repos.json.tmp`
    cat repos.json.tmp >> repos.json
    if [ "$URL" = "null" ]; then break; fi
done

jq -r '.values[] | select(.scm == "hg") | .name ' < repos.json > mercurial.names
