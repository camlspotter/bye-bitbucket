# BitBucket Mercurial migration

## Requirements

* `hg-git`
* Add `git=` to `[extensions]` in your `.hgrc`

## How to

1. Fix the scripts.
1. Run `list-repo.sh1
1. Run `clones.sh`

`clones.sh` clones only branches displayed by `hg branches`.  Unnamed heads and closed branches are not exported.
