#!/bin/sh

set -e

USER=`cat user.txt`

for i in `cat mercurial.names`
do
    echo '***********' $i
    /bin/rm -rf $i
    if [ ! -d $i ]; then
	hg clone https://$USER@bitbucket.org/camlspotter/$i

	# The body fails if the repo is completely empty
	if [ "`cd $i; hg log`" != "" ]; then
	    (cd $i; \
	     mkdir git; \
	     (cd git; git init); \
	     for b in `hg branches | awk '{ print $1 }'`; \
	     do \
		 echo '----------' $b; \
		 hg update $b; \
		 c=`echo $b | sed -e 's|/|-|g'`; \
		 hg bookmark hg-$c; \
	     done; \
	     hg push git)
	fi
    fi
done
